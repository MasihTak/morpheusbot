module.exports = (client, Discord) => {
    const image = new Discord.MessageAttachment('./images/Morpheus-pills.jpg');
    client.on('guildMemberAdd', (member) => {
        console.log(member.user.username + " joined!");

        const message = `At last!
Welcome, <@${member.id}> to the ${member.guild.name}!
As you no doubt have guessed, I'm Morpheus. 
Let me tell you why you're here. You're here because you know something. What you know, you can't explain, but you feel it. you felt it your entire life!
This's your last chance! after this, there's no turning back. you take the **blue** pill... the story ends, you redirect to the world of internet and believe whatever you want to believe! 
You take the **red** pill, you stay in Wonderland and I show you how to change the world!
(Red or Blue?)`
        member.send(message);
        member.send(image);
    })
}
