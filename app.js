//Import libraries and files
const dotenv = require('dotenv');
const Discord = require('discord.js');
const client = new Discord.Client();
const DM = require('./DM');
const MorpheusMessage = require('./MorpheusMessage');
const prefix = process.env.PREFIX;
dotenv.config();

// when the client is ready, run this code
// this event will only trigger one time
client.once('ready', () => {
    console.log('I am ready! 😎');
    DM(client, Discord);
    MorpheusMessage(client, Discord);
});

// Login to Discord with your app's token
client.login(process.env.TOKEN);
