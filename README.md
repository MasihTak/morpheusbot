# Morpheus Bot!
A discord bot based on the Matrix movie offers the new user 2 pills and based on their decision, they will either go to the developer's land! And unlock new channels, or they will kick from the server!

**Is it just a fun bot?**

No! there's a [philosophy](https://www.youtube.com/watch?v=tMg5s8SnxLs) behind it!



## TODO
 - Assign the 'developer' role when the user takes the red pill!
 - Kick the user if he/she take the blue pill!

## Project setup
```
npm install
```

### Environment variables
Create a `.env` file in your project root with these contents:
```
TOKEN= Your bot token here without spaces
PREFIX= prefix you like
```

### Awake bot
```
npm run start
```

## Sponsors
[![JetBrains Logo](jetbrains.svg)](https://www.jetbrains.com/?from=https://gitlab.com/MasihTak/morpheusbot)
[![BitNinja Logo](bitninja.svg)](https://bitninja.io)
