module.exports = (client, Discord ) => {
    const neoTakingRedPill = new Discord.MessageAttachment('./images/Neo-taking-red-pill.jpg')
    const matrixHasYou = 'https://tenor.com/view/matrix-gif-8222043'
    const worldOfProgrammers = 'https://tenor.com/view/the-matrix-reloaded-matrix-reloaded-neo-keanu-reeves-gif-4011236'

    client.on('message', message => {
        if (message.author.bot) return;
        if (message.channel.type === 'dm') {
            if (message.content.toUpperCase() === 'RED') {
                message.channel.send(neoTakingRedPill)
                const collector = new Discord.MessageCollector(message.channel, m => m.author.id === message.author.id, {time: 10000});
                message.channel.send(`Remember all I'm offering is the truth. Noting more! (Yes or No?) `)
                collector.on('collect', message => {
                    if (message.content.toUpperCase() === 'YES') {
                        message.channel.send("Welcome to the world of programmers!");
                        message.reply(worldOfProgrammers)
                    //    TODO: assign 'developer' role
                    } else if (message.content.toUpperCase() === 'NO') {
                        message.reply("I see!");
                        message.reply(matrixHasYou)
                    } else {
                        message.channel.send(`Terminated: Invalid Response`)
                    }
                })
            } // end red if
            else if (message.content.toUpperCase() === 'BLUE') {
                message.reply('NM!')
                //TODO: kick the user!
            }
        } // end dm
    }) // end client
}
